---
layout: handbook-page-toc
title: "Sales Kickoff"
description: "The GitLab Sales Kickoff sets the tone for the GitLab field organization for the new fiscal year"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Sales Kickoff (SKO) Overview
This event sets the tone for the GitLab field organization for the new fiscal year with a focus on the following goals:
1.  **Motivate**: Be energized about GitLab’s vision & strategy and the incredible opportunities ahead (for each and every GitLab team member, the company, our customers & partners)
1.  **Celebrate**: Recognize and enjoy the successes of the prior fiscal year
1.  **Enable**: Ensure every field team member returns home with a clear understanding of what’s needed to plan and execute their business for the new fiscal year (based on functional enablement priorities from senior leadership) 

----

## [Sales Kickoff 2020](/handbook/sales/training/SKO/2020)

## [Sales Kickoff 2021](/handbook/sales/training/SKO/2021)

## [Sales Kickoff 2022](/handbook/sales/training/SKO/2022)

----

## Sales Kickoff Planning 

For more information about the Sales Kickoff planning core team and process, see the [Sales Kickoff Planning](https://about.gitlab.com/handbook/sales/training/SKO/SKO-planning/) page.
